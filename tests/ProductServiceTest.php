<?php

use PHPUnit\Framework\TestCase;
use App\Service\ProductService;
use App\Repository\ProductRepository;
use App\Model\Product;

class ProductServiceTest extends TestCase
{
    public function testGetAllProducts()
    {
        $mockRepo = $this->createMock(ProductRepository::class);
        $mockRepo->method('getAllProducts')
                 ->willReturn([
                     new Product(1, 'Product 1', 99.99),
                     new Product(2, 'Product 2', 49.99)
                 ]);

        $service = new ProductService($mockRepo);

        $result = $service->getAllProducts();

        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertInstanceOf(Product::class, $result[0]);
        $this->assertInstanceOf(Product::class, $result[1]);
    }
}


