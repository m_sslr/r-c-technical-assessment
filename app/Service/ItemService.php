<?php

namespace App\Service;

use App\Repository\ItemRepository;

class ItemService
{
    public function __construct(private ItemRepository $productRepository)
    {

    }

    public function getAllItems(): array
    {
        return $this->productRepository->getAllItems();
    }

    public function createItem($name): void
    {
        $this->productRepository->createItem($name);
    }

    public function updateItem($id, $name, $done): void
    {
        $this->productRepository->updateItem($id, $name, $done);
    }

    public function deleteItem($id): void
    {
        $this->productRepository->deleteItem($id);
    }
}


