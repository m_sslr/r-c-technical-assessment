<?php

namespace App\Model;

class Item
{
    public function __construct(private $id, private $name, private $done)
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDone()
    {
        return $this->done;
    }
}


