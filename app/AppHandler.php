<?php

namespace App;

use DI\Container;
use GuzzleHttp\Psr7\ServerRequest;
use App\Router;
use FastRoute\Dispatcher;
use App\Middleware\RequestTimeMiddleware;
use GuzzleHttp\Psr7\Response;

class AppHandler
{
    private Router $router;
    private ResponseEmitter $responseEmitter;

    public function __construct(private Container $container)
    {
        $this->container = $container;
        $this->router = new Router($container);
        $this->responseEmitter = new ResponseEmitter();
    }

    public function run(): void
    {
        $dispatcher = $this->router->getDispatcher();
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        $routeInfo = $dispatcher->dispatch($httpMethod, $uri);

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                $response = new Response(404, [], 'Page not found');
                $this->responseEmitter->emit($response);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $response = new Response(500, [], 'Method not allowed');
                $this->responseEmitter->emit($response);
                break;
            case Dispatcher::FOUND:

                $request = ServerRequest::fromGlobals();

                // Get the middleware
                $handler = $routeInfo[1];
                $routeParams = $routeInfo[2];

                // Add route parameters to request attributes
                foreach ($routeParams as $key => $value) {
                    $request = $request->withAttribute($key, $value);
                }

                // Apply middleware and get the response
                $middleware = new RequestTimeMiddleware();
                $callableHandler = new CallableRequestHandler($handler);
                $response = $middleware->process($request, $callableHandler);

                // Emit the response
                $this->responseEmitter->emit($response);
                break;
        }
    }
}

