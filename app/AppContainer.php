<?php

namespace App;

use DI\ContainerBuilder;
use DI\Container;
use PDO;

class AppContainer
{
    public function getContainer(): Container
    {
        $containerBuilder = new ContainerBuilder;

        $containerBuilder->addDefinitions([
            PDO::class => function () {
                return new PDO(
                    'mysql:host=db;dbname=my_db;charset=utf8',
                    'db_user',
                    'db_password'
                );
            },
        ]);


        // Enable autowiring
        $containerBuilder->useAutowiring(true);
        return $containerBuilder->build();
    }
}


