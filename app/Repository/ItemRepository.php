<?php

namespace App\Repository;

use App\Model\Item;
use PDO;

class ItemRepository
{
    public function __construct(private PDO $pdo)
    {

    }

    public function getAllItems(): array
    {
        $stmt = $this->pdo->query('SELECT * FROM items');
        $items = [];
        while ($row = $stmt->fetch()) {
            $items[] = new Item($row['id'], $row['name'], $row['done']);
        }
        return $items;
    }

    public function createItem($name): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO items (name) VALUES (?)');
        $stmt->execute([$name]);
    }

    public function updateItem($id, $name, $done): void
    {
        $stmt = $this->pdo->prepare('UPDATE items SET name = ?, done = ? WHERE id = ?');
        $stmt->execute([$name, intval($done), $id]);
    }

    public function deleteItem($id): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM items WHERE id = ?');
        $stmt->execute([$id]);
    }
}
