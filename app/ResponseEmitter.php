<?php

namespace App;

use Psr\Http\Message\ResponseInterface;

class ResponseEmitter
{
    public function emit(ResponseInterface $response): void
    {
        // Emit status code
        http_response_code($response->getStatusCode());

        // Emit headers
        foreach ($response->getHeaders() as $header => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $header, $value), false);
            }
        }

        // Emit response body
        echo $response->getBody();
    }
}

