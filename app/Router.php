<?php

namespace App;

use DI\Container;
use FastRoute\RouteCollector;
use App\Controller\ItemController;
use function FastRoute\simpleDispatcher;

class Router
{
    public function __construct(private Container $container)
    {
        $this->container = $container;
    }

    public function getDispatcher(): \FastRoute\Dispatcher
    {
        return simpleDispatcher(function (RouteCollector $r) {
            $r->addRoute('GET', '/items', [$this->container->get(ItemController::class), 'getItems']);
            $r->addRoute('POST', '/items', [$this->container->get(ItemController::class), 'createItem']);
            $r->addRoute('PUT', '/items/{id:\d+}', [$this->container->get(ItemController::class), 'updateItem']);
            $r->addRoute('DELETE', '/items/{id:\d+}', [$this->container->get(ItemController::class), 'deleteItem']);
            $r->addRoute('GET', '/app', function() {
                $response = new \Zend\Diactoros\Response();
                $response->getBody()->write(file_get_contents(__DIR__.'/../public/app.html'));
                return $response;
            });
            $r->addRoute('GET', '/app.js', function() {
                header('Content-Type: application/javascript');
                include(__DIR__.'/../public/app.js');
            });
        });
    }
}


