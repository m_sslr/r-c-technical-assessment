<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Service\ItemService;
use Zend\Diactoros\Response;

class ItemController
{
    public function __construct(private ItemService $itemService)
    {

    }

    public function getItems(ServerRequestInterface $request)
    {
        $items = $this->itemService->getAllItems();

        $itemArray = array_map(function ($item) {
            return [
                'id' => $item->getId(),
                'name' => $item->getName(),
                'done' => $item->getDone()
            ];
        }, $items);

        $response = new Response();
        $response->getBody()->write(json_encode($itemArray));

        return $response->withHeader('Content-Type', 'application/json');
    }

    public function createItem(ServerRequestInterface $request): ResponseInterface
    {
        $data = json_decode($request->getBody()->getContents(), true);
        $this->itemService->createItem($data['name']);

        $response = new Response();
        $response->getBody()->write(json_encode(['message' => 'Item created successfully']));
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function updateItem(ServerRequestInterface $request): ResponseInterface
    {
        $id = $request->getAttribute('id'); 
        $data = json_decode($request->getBody()->getContents(), true);
        
        $this->itemService->updateItem($id, $data['name'], $data['done']);
    
        $response = new Response();
        $response->getBody()->write(json_encode(['message' => 'Item updated successfully']));
        return $response->withHeader('Content-Type', 'application/json');
    }
    
    public function deleteItem(ServerRequestInterface $request): ResponseInterface
    {
        $id = $request->getAttribute('id');
        $data = json_decode($request->getBody()->getContents(), true);
        $this->itemService->deleteItem($id);

        $response = new Response();
        $response->getBody()->write(json_encode(['message' => 'Item deleted successfully']));
        return $response->withHeader('Content-Type', 'application/json');
    }

}


