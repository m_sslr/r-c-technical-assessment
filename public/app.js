const app = Vue.createApp({
  data() {
    return {
      newItem: "",
      items: [],
      loading: true,
    };
  },
  async created() {
    this.getAllItems();
  },
  methods: {
    async getAllItems() {
      try {
        const response = await axios.get("/items");
        this.items = response.data;
        this.items.sort((a, b) => b.done - a.done);
      } catch (error) {
        console.error(error);
      }
      this.loading = false;
    },
    async addItem() {
      this.loading = true;
      if (this.newItem !== "") {
        try {
          const response = await axios.post("/items", { name: this.newItem });
          this.items.push(response.data);
          this.newItem = "";
        } catch (error) {
          console.error(error);
        }
      }
      this.getAllItems();
    },
    async deleteItem(id) {
      this.loading = true;

      try {
        await axios.delete(`/items/${id}`);
        this.items = this.items.filter((item) => item.id !== id);
      } catch (error) {
        console.error(error);
      }
      this.getAllItems();
    },
    async editItem(item) {
      if (!item.isEditing) {
        item.isEditing = true;
      }
    },
    async saveItem(item) {
      this.loading = true;
      item.isEditing = false;
      try {
        const response = await axios.put(`/items/${item.id}`, {
          name: item.name,
          done: item.done,
        });
        item.name = response.data.name;
        item.isEditing = false;
      } catch (error) {
        console.error(error);
      }
      this.getAllItems();
    },
    async toggleDoneItem(item) {
      this.loading = true;
      const response = await axios.put(`/items/${item.id}`, {
        done: !item.done,
        name: item.name,
      });
      item.done = response.data.done;
      item.name = response.data.name;
      this.getAllItems();
    },
  },
});

app.mount("#app");
