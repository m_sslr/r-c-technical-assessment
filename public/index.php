<?php

require __DIR__ . '/../vendor/autoload.php';

use App\AppContainer;
use App\AppHandler;

// Get DI container.
$container = (new AppContainer())->getContainer();

// Create a new Application Handler instance.
$app = new AppHandler($container);

// Run the Application
$app->run();

