PROJECT_NAME := r-c-technical-assessment
DOCKER_COMPOSE := COMPOSE_PROJECT_NAME=$(PROJECT_NAME) docker-compose
DOCKER_CONTAINER_PREFIX := $(PROJECT_NAME)

# Define targets and their respective commands
upd:
	$(DOCKER_COMPOSE) up -d $(filter-out $@,$(MAKECMDGOALS))

up:
	$(DOCKER_COMPOSE) up $(filter-out $@,$(MAKECMDGOALS))

recreate:
	$(DOCKER_COMPOSE) up -d --build --force-recreate $(filter-out $@,$(MAKECMDGOALS))

down:
	$(DOCKER_COMPOSE) down

composer-require:
	$(DOCKER_COMPOSE) exec app composer require $(filter-out $@,$(MAKECMDGOALS))

composer-remove:
	$(DOCKER_COMPOSE) exec app composer remove $(filter-out $@,$(MAKECMDGOALS))

bash-%:
	docker exec -it $(DOCKER_CONTAINER_PREFIX)-$*-1 bash

sh-%:
	docker exec -it $(DOCKER_CONTAINER_PREFIX)-$*-1 sh

test:
	docker exec r-c-technical-assessment-app-1 ./vendor/bin/phpunit tests 

migration:
	docker exec r-c-technical-assessment-app-1 vendor/bin/phinx create $(filter-out $@,$(MAKECMDGOALS))

dbmigrate:
	docker exec r-c-technical-assessment-app-1 vendor/bin/phinx migrate

dbseed:
	docker exec r-c-technical-assessment-app-1 vendor/bin/phinx seed:run

tree:
	tree -L 4 -I vendor

%:
	@:

