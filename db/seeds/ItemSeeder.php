<?php

use Phinx\Seed\AbstractSeed;

class ItemSeeder extends AbstractSeed
{
    public function run() : void
    {
        $data = [
            [
                'name'    => 'Bread',
            ],
            [
                'name'    => 'Water',
            ],
           
        ];

        $this->table('items')->insert($data)->save();
    }
}


